<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// ruta pública al login (iniciar sesión)
Route::post('login', [AuthController::class, 'login']);
Route::get('profile', [AuthController::class, 'profile']);

//rutas solo para usuarios 
// Route::group(['middleware' => ['auth:sanctum']], function () {
//     // Ruta para generar información del usuario
//     Route::get('/profile', [AuthController::class, 'profile']);

//     // ruta para logout (cerrar sesión)
//     Route::post('/logout', [AuthController::class, 'logout']);
// });

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

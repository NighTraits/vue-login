import {createWebHistory, createRouter} from 'vue-router';

import Login from '../components/Login.vue';
import Profile from '../components/Profile.vue';

export const routes = [
    {
        name:'login',
        path:'/',
        component: Login
    },
    {
        name:'profile',
        path:'/profile',
        component: Profile
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes: routes,
});

export default router;
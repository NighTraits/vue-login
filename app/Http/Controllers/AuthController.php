<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request){
            $fields = $request -> validate([
                'name' => 'required|string',
                'password' => 'required|string'
            ]);
    
            // busca el primer nombre de usuario que concuerde
            $user = User::WHERE('name', $fields['name'])->first();
    
            // revisa si el usuario y la contraseña son validas
            if(!$user || !Hash::check($fields['password'], $user->password)){
                $success = false;
                $message = "usuario o contraseña incorrecta";
                $token = null;
            }else {
                $token = $user->createToken('myapptoken')->plainTextToken;
                $success = true;
                $message = "Sesión iniciada";
            }
            
            $response = [
                'success' => $success,
                'message' => $message,
                'token' => $token
            ];
    
            return response()->json($response);
        }

        public function logout(Request $request){
            auth()->user()->tokens()->delete();
            return [
                'message' => 'Sesión cerrada'
            ];
        }

        public function profile(Request $request){
            $user = auth()->user();
            return User::find($user);
        }
    
}
